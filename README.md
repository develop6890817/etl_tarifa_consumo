
## Inicio del proyecto

Para el desarrollo del proyecto se recurrio a la estructura de una etl(extracion,transformacion y carga) para tener un manejo de 
trazabilidad y de los fallos que se presente en la manipulacion del dato en los estados que se lleve.

## Integracion y correr el proyecto local se debe:

- [ ] [se debe crear el entorno virtual .env]
- [ ] [dentro del entorno local se instala las dependencias que contramos en el requeriments.txt, dentro de la carpeta /src]
- [ ] [tener presente si el entorno no tiene controlador de mariadb instalar, o le genera error]
- [ ] [En memoria se tiene los 5 dataframe procesos , en el script main.py. los podemos encontrar]



## Funcionalidades

El proposito del apartado se realizar una entrega del producto que tiene como resultado
- tabla de ejecuccion por cada proceso que paso el dato
- los datos cargados a nivel de base de datos

## Estructura de las tablas

### tabla: tabla_ejeucion
 
- id : identificador autoincremental
- proceso: se define el proceso que va el dato
- nombre_dataframe: nombre del dataframe que se extrae el dato
- hora_inicio: se registra la hola de inicio de ejecuccion
- cantidad_nulos: cantidad de nulos que encuentra por columna
- cantidad_registros: cantidad de registros totales cargados
- cantidad_columnas: cantidad de columnas cargadas
- estado: define el estado del proceso si fue : exito o fallido
- hora_finalizacion: se registra la hora de finalizacion de la ejecuccion

![Ejemplo](https://gitlab.com/develop6890817/etl_tarifa_consumo/-/raw/main/ev_1.JPG?ref_type=heads)


### tabla: tabla_tarifa_consumo

En este apartado aquellos registros que no cumpli con a las condiciones de : minio o maximo fueron eliminados.

- id : identificador del registro
- año : año de la tarifa
- destino : destino 
- consumo : valor neto del consumo
- tarifa_consumo : porcentjae de la tasa ah camular
- tasa_consumo : la tasas calculada

![Ejemplo](https://gitlab.com/develop6890817/etl_tarifa_consumo/-/raw/main/ev_2.JPG?ref_type=heads)



## Consultas de evidencia

### cantidad de registros cargados

![Ejemplo](https://gitlab.com/develop6890817/etl_tarifa_consumo/-/raw/main/cantidad_registros.JPG?ref_type=heads)

### suma total de la tarifa de consumo calculada

![Ejemplo](https://gitlab.com/develop6890817/etl_tarifa_consumo/-/raw/main/suma_tasa_consumo.JPG?ref_type=heads)


## Authors and acknowledgment
Jose Manolo Pinzon Hernandez
