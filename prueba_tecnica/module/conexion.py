from sqlalchemy import create_engine,MetaData
from dotenv import load_dotenv
import pandas as pd
import os

load_dotenv()
def conectar():
    DB_HOST = os.getenv('DB_HOST')
    DB_USER = os.getenv('DB_USER')
    DB_PASSWORD = os.getenv('DB_PASSWORD')
    DB_NAME = os.getenv('DB_NAME')
    try:
        engine = create_engine(f"mariadb:///?User={DB_USER}&Password={DB_PASSWORD}&Database={DB_NAME}&Server={DB_HOST}&Port=3306")
        print(engine)
        print("Conexión exitosa a la base de datos")
        return engine
    except Exception as e:
        print("Error al conectar a la base de datos:", e)
        return None


def insertar_datos_tabla(nombre_tabla,pd_txt):
    meta = MetaData()
    conector = conectar()
    meta.reflect(bind=conector)
    if nombre_tabla in meta.tables:
        pd_txt.to_sql("estado_consumo", con=conector, if_exists='replace', index=False)
        return
    pd_txt.to_sql("estado_consumo", con=conector, if_exists='append', index=False)
