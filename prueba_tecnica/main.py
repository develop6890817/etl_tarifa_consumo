from orquestador import orquestador
from module import conexion
import pandas as pd
import os

ruta_script = os.path.dirname(os.path.abspath(__file__))
ruta_carpeta2 = os.path.join(ruta_script, "..", "resource")
arreglo_datos = []
arreglo_datos_auxiliares = []
if os.path.exists(ruta_carpeta2):
    contenido_carpeta2 = os.listdir(ruta_carpeta2)
    for elemento in contenido_carpeta2:
        ruta = f'{ruta_carpeta2}\\{elemento}'
        if "txt" in elemento :
            arreglo_datos.append(ruta)
        else:
            arreglo_datos_auxiliares.append(ruta)
else:
    print("Error")

objeto_proceso = orquestador()
list_dataframe = []
for item in arreglo_datos:
    dataframe = objeto_proceso.data(item,arreglo_datos_auxiliares)
    list_dataframe.append(dataframe)

del objeto_proceso
