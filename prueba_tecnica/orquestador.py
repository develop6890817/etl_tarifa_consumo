from src.extracion.main import mainExtr
from src.transformacion.main import mainTransf
from src.carga.main import mainCarga
import pandas as pd

class orquestador:
    
    def __init__(self) -> None:
        pass
    
    def data(self,nombre_dataframe,datos_auxiliares):
        ruta = nombre_dataframe
        dicc_transformacion = []
        nombre_dataframe = nombre_dataframe.split('\\')[-1]
        extracion = mainExtr(ruta, nombre_dataframe)
        extracion_dataframe = extracion.get_dataframe()
        dicc_extracion = extracion.get_dicc_extracion()
        if dicc_extracion['estado'][0]=='exito':
            transformacion = mainTransf(extracion_dataframe,nombre_dataframe,datos_auxiliares)
            transformacion_dataframe = transformacion.get_dataframe()
            dicc_transformacion = transformacion.get_dicc_transformacion()
        
        if dicc_transformacion['estado'][0]=='exito':
            dicc_carga, carga_dataframe = mainCarga(transformacion_dataframe,nombre_dataframe)
            
        return carga_dataframe

