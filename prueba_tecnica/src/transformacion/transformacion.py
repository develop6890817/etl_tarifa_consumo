
import pandas as pd
import os
from unidecode import unidecode
from datetime import datetime

class Transformacion():
    
    def __init__(self, dataframe,dic_transformacion,datos_auxiliares) -> None:
        """
        Method: Constructor de la clase que inicializa el dataframe y las
        variables necesarias para la transformacion
        
        """
        self.maximos = pd.read_csv(datos_auxiliares[0])
        self.minimimos = pd.read_csv(datos_auxiliares[1]) 
        self.tarifas = pd.read_csv(datos_auxiliares[2])
        self.pd = dataframe
        self.dic_transformacion = dic_transformacion
        

    def transformacion_dato_esquema(self):
        """
        Method: Realiza la construccion del dataframe y la carga a nivel
        de dato del esquema que actualmente se encuentra la informacion
        
        """
        self.pd = self.pd.drop('minimo', axis=1)
        self.pd = self.pd.drop('maximo', axis=1)
        nulos_por_columna = self.pd.isnull().sum().sum()
        self.dic_transformacion["cantidad_nulos"] =[ nulos_por_columna]
        self.dic_transformacion["cantidad_registros"] =[ self.pd.shape[0]]
        self.dic_transformacion["cantidad_columnas"] =[ self.pd.shape[1]]
    
    def unir_dataframes(self):
        self.tarifas = self.tarifas.rename(columns={'Destino': 'destino', 'Estrato': 'estrato','Tarifa sobre consumo':'tarifa_consumo'})
        self.tarifas["destino"] = self.tarifas["destino"].apply(lambda x: unidecode(x).lower().replace(" ",""))
        self.pd["destino"] = self.pd["destino"].apply(lambda x: unidecode(x).lower().replace(" ",""))
        df_resultado = pd.merge(self.pd, self.tarifas, on=['destino', 'estrato'], how='outer')
        df_resultado["tarifa_consumo"] = df_resultado["tarifa_consumo"].apply(lambda x: self.conversion_entero(x))
        df_resultado["tasa_consumo"] = df_resultado.apply(lambda x: self.calcular_tasa(x),axis=1)
        return df_resultado

    def conversion_entero(self,columna):
        try:
            valor =  columna.split("%")[0].replace(",",".")
            valor =  float(valor) / 100
            return valor
        except Exception as e:
            return 0
    
    def calcular_tasa(self,columna):
        try:
            return columna["consumo"] * columna["tarifa_consumo"]
        except Exception as e:
            return 0
    
    def condiciones_min(self,resultado_data):
        self.minimimos = self.minimimos.rename(columns={'Año': 'año', 'Mínimo': 'minimo'})
        df_resultado_minimo = pd.merge(resultado_data, self.minimimos, on=['año'], how='outer')
        df_resultado_minimo = df_resultado_minimo.dropna(subset=['minimo'], how='any')
        df_resultado_minimo['minimo'] = df_resultado_minimo['minimo'].apply(lambda x: x.replace(".","").replace(",","."))
        df_resultado_minimo['minimo'] = df_resultado_minimo['minimo'].astype(float)
        condicion = (df_resultado_minimo['tasa_consumo'] >= df_resultado_minimo['minimo'])
        df_resultado_minimo = df_resultado_minimo.loc[condicion]
        return df_resultado_minimo
         
    def condiciones_max(self,resultado_data):
            self.maximos = self.maximos.rename(columns={'Año': 'año', 'Máximo': 'maximo','Destino':'destino'})
            self.maximos["destino"] = self.maximos["destino"].apply(lambda x: unidecode(x).lower().replace(" ",""))
            df_resultado_maximo = pd.merge(resultado_data, self.maximos, on=['año','destino'], how='outer')
            df_resultado_maximo = df_resultado_maximo.dropna(subset=['maximo'], how='any')
            df_resultado_maximo['maximo'] = df_resultado_maximo['maximo'].apply(lambda x: x.replace(".","").replace(",","."))
            df_resultado_maximo['maximo'] = df_resultado_maximo['maximo'].astype(float)
            condicion = (df_resultado_maximo['tasa_consumo'] <= df_resultado_maximo['maximo'])
            df_resultado_maximo = df_resultado_maximo.loc[condicion]
            return df_resultado_maximo
        
    
    def ejecuccion_transformacion(self):
        """
        Method: Orquesta los metodos encargados de la transformacion
        
        """
        resultado_data = self.unir_dataframes()
        resultado_data_min = self.condiciones_min(resultado_data)
        resultado_data_max = self.condiciones_max(resultado_data_min)
        self.pd = resultado_data_max
        self.transformacion_dato_esquema()
        return self.pd,self.dic_transformacion
       
