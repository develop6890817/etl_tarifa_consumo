
import pandas as pd
class Extracion():
    
    def __init__(self,dic_extracion,ruta) -> None:
        """
        Method: Constructor de la clase que inicializa el dataframe y las
        variables necesarias para la extracion
        
        """
        self.archivo_txt  = ruta
        self.pd_txt = pd.DataFrame
        self.esquema_dato_crudo = dic_extracion
       
    def extracion_dato_esquema(self):
        """
        Method: Realiza la construccion del dataframe y la carga a nivel
        de dato del esquema que actualmente se encuentra la informacion
        
        """
        self.pd_txt = pd.read_csv(self.archivo_txt)
        nulos_por_columna = self.pd_txt.isnull().sum().sum()
        self.esquema_dato_crudo["cantidad_nulos"] = [nulos_por_columna]
        self.esquema_dato_crudo["cantidad_registros"] = [self.pd_txt.shape[0]]
        self.esquema_dato_crudo["cantidad_columnas"] = [self.pd_txt.shape[1]]
        
        
    def ejecuccion_extracion(self):
        """
        Method: Orquesta los metodos encargados de la extracion
        
        """
        self.extracion_dato_esquema()
        return self.pd_txt,self.esquema_dato_crudo
       
