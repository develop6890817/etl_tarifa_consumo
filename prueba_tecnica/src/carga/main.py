from src.carga.carga import Carga
from datetime import datetime
import pandas as pd
from module import conexion

class mainCarga():
    
    def __init__(self,dataframe,nombre_dataframe) -> None:
        """
        Method: Constructor de la clase que inicializa la orquestacion de la carga de los datos
        
        """
        self.dic_carga = {}
        self.dic_carga["proceso"] = ["carga"]
        self.dic_carga["nombre_dataframe"] = [nombre_dataframe]
        fecha_inicio = datetime.now()
        self.dic_carga["hora_inicio"] = [fecha_inicio.strftime("%Y-%m-%d %H:%M:%S")]
        self.dic_carga["cantidad_nulos"] = [0]
        self.dic_carga["cantidad_registros"] = [0]
        self.dic_carga["cantidad_columnas"] = [0]
        try:
            tercera_ejecuccion = Carga(dataframe,self.dic_carga)
            self.dic_carga, self.pd_txt  = tercera_ejecuccion.ejecuccion_carga()
            self.dic_carga["estado"] = ["exito"]
            self.dic_carga["hora_finalizacion"] = [datetime.now().strftime("%Y-%m-%d %H:%M:%S")]
            
        except Exception as e:
            print(e)
            self.dic_carga["estado"] = "fallido"
            self.dic_carga["hora_finalizacion"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        dicionario = pd.DataFrame(self.dic_carga)
        conexion.insertar_datos_tabla("tabla_ejecucion",dicionario)

        
    def get_dataframe(self):
        """
        Method: devuelve el dataframe de los cargados
        
        """
        return self.pd_txt
    
    def get_dicc_carga(self):
        """
        Method: devuelve el diccionaro de los datos cargados
        
        """
        return self.dic_carga 