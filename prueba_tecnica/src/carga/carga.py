
import pandas as pd
from module import conexion
class Carga():
    
    def __init__(self,dataframe,dic_carga) -> None:
        """
        Method: Constructor de la clase que inicializa el dataframe y las
        variables necesarias para la carga
        
        """
        self.pd_txt = dataframe
        self.dic_carga = dic_carga
       
    def carga_dato_esquema(self):
        """
        Method: Realiza la construccion del dataframe y la carga a nivel
        de dato del esquema que actualmente se encuentra la informacion
        
        """
        nulos_por_columna = self.pd_txt.isnull().sum().sum()
        self.dic_carga["cantidad_nulos"] = [nulos_por_columna]
        self.dic_carga["cantidad_registros"] = [self.pd_txt.shape[0]]
        self.dic_carga["cantidad_columnas"] = [self.pd_txt.shape[1]]
        
   
    def ejecuccion_carga(self):
        """
        Method: Orquesta los metodos encargados de la carga
        
        """
        self.carga_dato_esquema()
        conexion.insertar_datos_tabla("tarifa_consumo",self.pd_txt)
        return self.dic_carga, self.pd_txt